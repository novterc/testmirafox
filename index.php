<?php

require 'data.php';

$invalidReasonListByCommands = dataValidation($data);
if (!empty($invalidReasonListByCommands)) {
    throw new \Exception('imported data is invalid');
}

$middleAttackAndDefense = callMiddleAttackAndDefense($data);
$middleScore = callMiddleScore($data);


function dataValidation(array $data)
{
    $invalidReasonListByCommands = [];
    foreach ($data as $commandId => $commandData) {
        $invalidReasonList = dataValidationByCommand($commandData);
        if (!empty($invalidReasonList)) {
            $invalidReasonListByCommands[$commandId] = $invalidReasonList;
        }
    }

    return $invalidReasonListByCommands;
}

function dataValidationByCommand(array $commandData)
{
    $invalidReasonList = [];
    if (!isset($commandData['games']) || !is_numeric($commandData['games'])) {
        $invalidReasonList[] = 'the "games" attribute not exist or not integer';
    }

    if (!isset($commandData['goals']) || !is_array($commandData['goals'])) {
        $invalidReasonList[] = 'the "games" attribute not exist or not array';
    } else {
        if (!isset($commandData['goals']['scored']) || !is_numeric($commandData['goals']['scored'])) {
            $invalidReasonList[] = 'the "goals-scored" attribute not exist or not integer';
        }

        if (!isset($commandData['goals']['skiped']) || !is_numeric($commandData['goals']['scored'])) {
            $invalidReasonList[] = 'the "goals-skipped" attribute not exist or not integer';
        }
    }

    return $invalidReasonList;
}

function callMiddleAttackAndDefense(array $data)
{
    $middleAttackAndDefense = [
        'attack' => 0,
        'defense' => 0,
    ];

    $countCommand = count($data);
    if ($countCommand !== 0) {
        $sumAttack = 0;
        $sumDefense = 0;
        foreach ($data as $commandData) {
            $attackAndDefenseByCommand = callAttackAndDefenseByCommand($commandData);
            $sumAttack += $attackAndDefenseByCommand['attack'];
            $sumDefense += $attackAndDefenseByCommand['defense'];
        }
        $middleAttackAndDefense['attack'] = $sumAttack / $countCommand;
        $middleAttackAndDefense['defense'] = $sumDefense / $countCommand;
    }

    return $middleAttackAndDefense;
}

function callAttackAndDefenseByCommand(array $commandData)
{
    $returnList = [
        'attack' => callAttackByCommand($commandData),
        'defense' => callDefenseByCommand($commandData),
    ];

    return $returnList;
}

function callAttackByCommand(array $commandData)
{
    $games = (int)$commandData['games'];
    $goalsScored = (int)$commandData['goals']['scored'];

    if (empty($games)) {
        return 0;
    }
    $callResult = $goalsScored / $games;

    return $callResult;
}

function callDefenseByCommand(array $commandData)
{
    $games = (int)$commandData['games'];
    $goalsSkipped = (int)$commandData['goals']['skiped'];

    if (empty($games)) {
        return 0;
    }
    $callResult = $goalsSkipped / $games;

    return $callResult;
}

function callPowerAttackAndDefense(array $data, array $middleAttackAndDefense)
{
    $powerAttackAndDefenseListByCommand = [];
    foreach ($data as $commandId => $commandData) {
        $powerAttackAndDefenseListByCommand[$commandId] = callPowerAttackAndDefenseByCommand($commandData, $middleAttackAndDefense);
    }

    return $powerAttackAndDefenseListByCommand;
}

function callPowerAttackByCommand(array $commandData, array $middleAttackAndDefense)
{
    $powerAttack = 0;

    $attackAndDefense = callAttackAndDefenseByCommand($commandData);
    if ($middleAttackAndDefense['attack'] !== 0) {
        $powerAttack = $attackAndDefense['attack'] / $middleAttackAndDefense['attack'];
    }

    return $powerAttack;
}

function callPowerDefenseByCommand(array $commandData, array $middleAttackAndDefense)
{
    $powerDefense = 0;

    $attackAndDefense = callAttackAndDefenseByCommand($commandData);
    if ($middleAttackAndDefense['attack'] !== 0) {
        $powerDefense = $attackAndDefense['defense'] / $middleAttackAndDefense['defense'];
    }

    return $powerDefense;
}

function callMiddleScore(array $data)
{
    $totalScore = 0;
    $totalGames = 0;
    foreach ($data as $commandData) {
        $totalScore += $commandData['goals']['scored'];
        $totalGames += $commandData['games'];
    }
    if ($totalGames > 0) {
        $middleScore = $totalScore / $totalGames;
    } else {
        $middleScore = 0;
    }

    return $middleScore;
}

function match(int $firstCommandId, int $secondCommandId)
{
    $firstCommandData = getDataCommandById($firstCommandId);
    $secondCommandData = getDataCommandById($secondCommandId);

    $firstGoalsCount = callGoalsCount($firstCommandData, $secondCommandData);
    $secondGoalsCount = callGoalsCount($secondCommandData, $firstCommandData);

    return [$firstGoalsCount, $secondGoalsCount];
}

function callGoalsCount(array $firstCommandData, array $secondCommandData)
{
    global $middleAttackAndDefense, $middleScore;

    $firstPowerAttack = callPowerAttackByCommand($firstCommandData, $middleAttackAndDefense);
    $secondPowerDefense = callPowerDefenseByCommand($secondCommandData, $middleAttackAndDefense);

    $goalsCount = round($firstPowerAttack * $secondPowerDefense * $middleScore);

    return $goalsCount;
}

function getDataCommandById(int $commandId)
{
    global $data;

    if (!isset($data[$commandId])) {
        throw new Exception('the command index is not exist');
    }

    return $data[$commandId];
}
